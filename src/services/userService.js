const {User} = require("../models/userModel");

const getUserProfileInfo = async (userId) => {
  try {
    const userInfo = await User.findById(userId);
    return {
      "_id": userInfo._id,
      "username": userInfo.username,
      "createdDate": userInfo.createdAt,
    };
  } catch (error) {
    return false;
  }
};

const generateNewPassword = async (userId, oldPassword, newPassword) => {
  const user = await User.findById(userId);

  if (!user || oldPassword !== user.password) {
    return false;
  }

  await User.findByIdAndUpdate(userId, {$set: {
    password: newPassword,
  }});
};

const deleteUserProfile = async (userId) => {
  await User.remove({_id: userId});
};

module.exports = {
  getUserProfileInfo,
  deleteUserProfile,
  generateNewPassword,
};
