const {Truck} = require("../models/truckModel");
const {ClientError} = require("../errors");

const getTruck = async (userId) => {
  return await Truck.find({created_by: userId});
};

const addTruck = async (userId, truckPayload) => {
  const truck = new Truck({created_by: userId, ...truckPayload});
  await truck.save();
};

const getTruckById = async (userId, id) => {
  try {
    return await Truck.findOne({userId, _id: id});
  } catch (error) {
    return false;
  }
};

const updateTruckById = async (userId, id, truckPayload) => {
  try {
    await Truck.findOneAndUpdate({userId: userId, _id: id}, {
      $set: {...truckPayload},
    });
  } catch (error) {
    throw new ClientError("Wrong truck id");
  }
};

const deleteTruckById = async (userId, id) => {
  try {
    await Truck.deleteOne({userId: userId, _id: id});
  } catch (error) {
    throw new ClientError("Wrong truck id");
  }
};

const assignTruckById = async (userId, truckId) => {
  try {
    await Truck.findOneAndUpdate({created_by: userId, _id: truckId}, {
      assigned_to: userId,
    });
  } catch (error) {
    throw new ClientError("Wrong truck id");
  }
};


module.exports = {
  getTruck,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
