const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const {ClientError, ServerError} = require("../errors");

const {User} = require("../models/userModel");

const signUp = async ({email, password, role}) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });
  await user.save();
};

const signIn = async ({email, password}) => {
  const user = await User.findOne({email});

  if (!user || !(await bcrypt.compare(password, user.password))) {
    throw new ClientError("Wrong email or password");
  }

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
  }, "secret");
  return token;
};

const generatePasswordReset = async ({email}) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new ServerError("User with such email doesn't exist");
  }
};

module.exports = {
  signUp,
  signIn,
  generatePasswordReset,
};
