const express = require("express");
const router = new express.Router();

const {
  getUserProfileInfo,
  deleteUserProfile,
  generateNewPassword,
} = require("../services/userService");

router.delete("/", async (req, res) => {
  try {
    const {userId} = req.user;

    await deleteUserProfile(userId);

    res.status(200).json({message: ("Success")});
  } catch (error) {
    res.status(500).json({message: error.message});
  }
});

router.get("/", async (req, res) => {
  try {
    const {userId} = req.user;

    const user = await getUserProfileInfo(userId);
    if (user === false) {
      res.status(400).json({message: "no info about this user"});
    } else {
      res.json({user});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
});

router.patch("/password", async (req, res) => {
  try {
    const {userId} = req.user;
    const {
      oldPassword,
      newPassword,
    } = req.body;

    const resp = await generateNewPassword(userId, oldPassword, newPassword);
    if (resp === false) {
      res.status(400)
          .json({message: "User does not exist"});
    } else {
      res.status(200)
          .json({message: "Success"});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
});

module.exports = {
  userRouter: router,
};
