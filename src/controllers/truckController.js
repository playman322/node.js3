const express = require("express");
const router = new express.Router();

const {
  getTruck,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require("../services/truckService");

router.get("/", async (req, res) => {
  try {
    const {userId} = req.user;
    const truck = await getTruck(userId);
    res.status(200).json({trucks: truck});
  } catch (error) {
    res.status(500).json({message: error.message});
  }
});


router.get("/:id", async (req, res) => {
  try {
    const {userId} = req.user;
    const id = req.params.id;

    const truck = await getTruckById(userId, id);
    if (truck === false) {
      res.status(400).json({message: "Wrong truck id"});
    } else {
      res.json({truck});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
});

router.post("/", async (req, res) => {
  const {userId} = req.user;

  await addTruck(userId, req.body);

  res.json({message: "Truck added successfully"});
});


router.put("/:id", async (req, res) => {
  try {
    const {userId} = req.user;
    const id = req.params.id;

    await updateTruckById(userId, id, req.body);

    res.status(200).json({message: "Success"});
  } catch (error) {
    if (error.status === 400) {
      res.status(400).json({message: error.message});
    } else {
      res.status(500).json({message: error.message});
    }
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const {userId} = req.user;
    const id = req.params.id;

    await deleteTruckById(userId, id);

    res.status(200).json({message: "Success"});
  } catch (error) {
    if (error.status === 400) {
      res.status(400).json({message: error.message});
    } else {
      res.status(500).json({message: error.message});
    }
  }
});

router.post("/:id/assign", async (req, res) => {
  try {
    const {userId} = req.user;
    const id = req.params.id;

    await assignTruckById(userId, id);

    res.status(200).json({"message": "Success"});
  } catch (error) {
    if (error.status === 400) {
      res.status(400).json({message: error.message});
    } else {
      res.status(500).json({message: error.message});
    }
  }
});

module.exports = {
  truckRouter: router,
};
