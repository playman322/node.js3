const express = require("express");
const router = new express.Router();
const validator = require("email-validator");
const {ClientError} = require("../errors");


const {
  signUp,
  signIn,
  generatePasswordReset,
} = require("../services/authService");

router.post("/register", async (req, res) => {
  try {
    const {
      email,
      password,
      role,
    } = req.body;
    if (!(validator.validate(email))) {
      throw new ClientError("Incorrect email!");
    }
    await signUp({email, password, role});

    res.json({message: "Profile created successfully!"});
  } catch (error) {
    if (error.status === 400) {
      res.status(400).json({message: error.message});
    } else {
      res.status(500).json({message: error.message});
    }
  }
});

router.post("/login", async (req, res) => {
  try {
    const {
      email,
      password,
    } = req.body;

    const token = await signIn({email, password});

    res.status(200).json({"jwt_token": token});
  } catch (error) {
    if (error.status === 400) {
      res.status(400).json({message: error.message});
    } else {
      res.status(500).json({message: error.message});
    }
  }
});

router.post("/forgot_password", async (req, res) =>{
  try {
    const {
      email,
    } = req.body;
    await generatePasswordReset({email});

    res.status(200)
        .json({"message": "You have received your password on email"});
  } catch (error) {
    if (error.status === 400) {
      res.status(400).json({message: error.message});
    } else {
      res.status(500).json({message: error.message});
    }
  }
});

module.exports = {
  authRouter: router,
};
