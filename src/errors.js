class RequestError extends Error {
  constructor(status, message) {
    super(message);
    this.status = status;
  }
}

class ClientError extends RequestError {
  constructor(message) {
    super(400, message);
  }
}

class ServerError extends RequestError {
  constructor(message) {
    super(500, message);
  }
}

module.exports = {
  ClientError,
  ServerError,
};
