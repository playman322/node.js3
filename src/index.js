const express = require("express");
const morgan = require("morgan");
const mongoose = require("mongoose");
const app = express();
const PORT = 8080;

const {truckRouter} = require("./controllers/truckController");
const {authRouter} = require("./controllers/authController");
const {userRouter} = require("./controllers/userController");

const {middleware} = require("./middlewares/middleware");

app.use(express.json());
app.use(morgan("tiny"));

app.use("/api/auth", authRouter);

app.use(middleware);
app.use("/api/trucks", truckRouter);

app.use("/api/users/me", userRouter);


const start = async () => {
  try {
    await mongoose.connect("mongodb+srv://Niekrasov:magika@cluster0.77gwd.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {
      useNewUrlParser: true, useUnifiedTopology: true,
    });
    app.listen(PORT);
  } catch (error) {
    console.error(`Error on server startup: ${error.message}`);
  }
};

start();

app.use(errorHandler);

// eslint-disable-next-line require-jsdoc
function errorHandler(error, req, res, next) {
  console.log(error);
  res.status(500).send({"message": "Server error"});
}
