const jwt = require("jsonwebtoken");

const middleware = (req, res, next) => {
  const {authorization} = req.headers;

  if (!authorization) {
    return res.status(401)
        .json({message: "Provide authorization header"});
  }

  const [, token] = authorization.split(" ");

  if (!token) {
    return res.status(401).json({message: "Include token to request"});
  }

  try {
    const tokenPayload = jwt.verify(token, "secret");
    req.user = {
      userId: tokenPayload._id,
      email: tokenPayload.email,
    };
    next();
  } catch (error) {
    res.status(401).json({message: error.message});
  }
};

module.exports = {
  middleware,
};
