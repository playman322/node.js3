const mongoose = require("mongoose");

const Truck = mongoose.model("Truck", {

  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },

  assigned_to: String,

  type: {
    type: String,
    required: true,
    enum: ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"],
  },

  status: {
    type: String,
    enum: ["OL", "IS"],
    default: "IS",
  },


});

module.exports = {Truck};
