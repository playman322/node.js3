const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
    
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    
    assigned_to : String,

    status: {
        type: String,
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED']
    },

    state: {
        type: String,
        enum: [ 'En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery' ]
    },

    name: {
        type: String,
        required: true
    },

    payload: {
        type: Number,
        required: true
    },

    pickup_address: {
        type: string,
        required: true
    },

    delivery_address: {
        type: string,
        required: true
    },

    dimensions: {
        width: Number,
        length:	Number,
        height:	Number,
        required: true      
    },

    type: {
        type: String,
        required: true,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']
    },

    logs: 	[{
        message: String,
        time:{
            type: Date,
            default: Date.now()
        }
    }],

    created_date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { Load };
